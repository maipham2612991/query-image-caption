data = dict()
import json

with open('caption.txt', 'r') as file:
    while True:
        while True:
            images = file.readline()
            if 'output--' in images or not images:
                break

        if not images:
            break
        caption = file.readline()

        images = images.strip()
        caption = caption.strip()
        data[caption] = images


data_list = []
for i, (key, value) in enumerate(data.items()):
    data_list += [{
        'id': i,
        'caption': key,
        'images_path': value
    }]

with open('webapp/data.json', 'w') as file:
    json.dump(data_list, file)

