import os.path

from flask import Flask, render_template, flash, request

from model import Model

app = Flask(__name__)
app.secret_key = "123456"
model = Model()


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/query', methods=["POST", 'GET'])
def query():
    query_input = str(request.form['query_input'])
    flash("Here are the result for : " + query_input)

    images = [os.path.join("images", image) for image in model.querytopk(query_input)]
    if len(images) == 0:
        flash("No images related found")
    return render_template("index.html", images=images)


if __name__ == '__main__':
    app.run(debug=True)

