import torch
from flask.views import View
from sentence_transformers import SentenceTransformer, util
import json
import gensim.downloader as api
from gensim.models import Word2Vec
from sklearn.metrics.pairwise import cosine_similarity
from nltk.tokenize import word_tokenize
import numpy as np


class ImageInfo:
    def __init__(self, id, caption, image_path):
        self.id = id
        self.caption = caption
        self.image_path = image_path

    @staticmethod
    def from_dict(data_dict):
        return ImageInfo(
            id=data_dict.get('id'),
            caption=data_dict.get('caption'),
            image_path=data_dict.get('images_path')
        )


class Model(View):
    def __init__(self):
        self.model = api.load("word2vec-google-news-300")
        with open('data.json', 'r') as file:
            data = json.load(file)
        self.image_infos = [ImageInfo.from_dict(data_dict) for data_dict in data]
        self.captions_embedding = np.array([self.preprocess_and_embed_text(image.caption)
                                            for image in self.image_infos])

    def preprocess_and_embed_text(self, text):
        words = word_tokenize(text.lower())
        words = [word for word in words if word in self.model]
        return np.mean([self.model[word] for word in words], axis=0)

    def querytopk(self, query, k=20, threshold=0.25):
        query_embedding = self.preprocess_and_embed_text(query)
        similarity = cosine_similarity(query_embedding.reshape(1, -1), self.captions_embedding)[0]
        top_k_indices = np.argsort(similarity)[::-1][:k]
        return [self.image_infos[index].image_path for index in top_k_indices]