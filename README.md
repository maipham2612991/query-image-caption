# Query Image Caption

## Task

Input: Given a set of images and their captions: `data.json` and a query <br>
Output: images most related to the query

## Approach

- Use pretrain model Word2Vec: `word2vec-google-news-300`
- Use flask to a simple build website

